import React, { Component } from 'react'
import './Todo.css'

export default class TableComponent extends Component {
    render() {
        return (
            <tr>
            <td>{this.props.todoInfo.id}</td>
            <td>{this.props.todoInfo.title}</td>
            <td className={this.props.todoInfo.completed === false? "marker-not-complete":"marker_complete"}>{this.props.todoInfo.completed===false? "Not Completed":"Completed"}</td>
            
                
    </tr>
    
);
}
}

