import React, { Component } from 'react';
import TableComponents from './TableComponent'

class TodoList extends Component {
    render() {
        return (
            <div class="table-responsive-md">
         <div style={{display:"flex", alignItems:"center", flexDirection:"column"}}>
           <table class="table table-bordered" style = {{width:500, border: '2px solid black'}}>
               <thead class="table-info" style={{border: '1px solid black'}}>
                   <th>ID</th>
                   <th>Title</th>
                   <th>Status</th>
                   
               </thead>
               <tbody>
                   {this.props.data.map((todo, index) =>{
                       return <TableComponents todoInfo={todo} key={index}/>
                   })}
               </tbody>
           </table>
        </div>
            </div>
        );
    }
}

export default TodoList;
