import logo from './logo.svg';
import './App.css';
import FetchHandler from './components/FetchHandler';

function App() {
  return (
    <div className="App">
      <FetchHandler/>
    </div>
  );
}

export default App;
